
-- SUMMARY --
  This module is going to tell more detailed about Available and Unavailable of modules and themes for Drupal 8. And also it will show HOOKS API which are typed in custom module.
  

-- INSTALLATION --

  Hopefully, you know the drill by now :)
  1. Download the module and extract the files.
  2. Upload the entire Advanced upgrade status into your Drupal 
	 sites/all/modules/ or sites/my.site.folder/modules/ directory if you are 
	running a multi-site installation of Drupal and you want this module to be
    specific to a particular site in your installation.
  3. Enable the Advanced upgrade status module by navigating to:
     Administration > Modules
  4. Goto Report->Available updates->Advanced Upgrade Status.
  5. If you want to download report as csv, then click "Download as CSV".

-- USAGE --
	It shows the available and unavailable of modules and themes.

-- Color Details --
  
  For Unavailable Module/Theme table:
    1. Red : There is no available modules and themes for drupal 8.

  For Modules/Themes available for upgrade table:
    1. Red : The Module which is recently released for Development.
    2. Yellow : The Module which is recently released for Other like rc, alpha and beta .
    3. Green : The Module which is recently released for Supported.

  For Custom Modules table:
    1. Yellow : Custom modules having hooks Api.